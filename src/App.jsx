import { useEffect, useState } from "react";
import Modal from "./components/Modal/Modal"
import  "./App.css"
import Header from "./components/Header/Header";
import PageContainer from "./components/PageContainer/PageContainer";
import ProductList from "./components/ProductList/ProductList";

function App() {

	const [firstModalOpen, setFirstModalOpen] = useState(false);
	const [selectedProduct, setSelectedProduct] = useState(false);

	
	const openFirstModal = (product) => {
		setFirstModalOpen(true);
		setSelectedProduct(product);
	};
	
	const closeFirstModal = () => {
		setFirstModalOpen(false);
		setSelectedProduct(false);
    };


	const [cartItems, setCartItems] = useState([]);
    
    const addToCart = () => {
        setCartItems(prevCartItems => [...prevCartItems, selectedProduct]);
	};

    useEffect(() => {
        const cart = JSON.parse(localStorage.getItem("cart")) || [];
        setCartItems(cart);
    }, []);

    useEffect(() => {
        localStorage.setItem("cart", JSON.stringify(cartItems));
	}, [cartItems]);
	

    const [cartsCount, setCartsCount] = useState()
	
	useEffect(() => {
        const carts = JSON.parse(localStorage.getItem("cart")|| []);
        setCartsCount(carts.length);
	}, [cartItems]);
	

	const [favorites, setFavorites] = useState([]);

	const addToFavorites = (product) => {
		const updatedProduct = { ...product, isFavorite: !product.isFavorite };

		setFavorites(prevFavorites => {
			const existingProductIndex = prevFavorites.findIndex(item => item.id === updatedProduct.id);

			if (existingProductIndex !== -1) {
				prevFavorites.splice(existingProductIndex, 1);
			} else {
				prevFavorites.push(updatedProduct);
			}

			return [...prevFavorites];
		});
	};

	useEffect(() => {
        const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
        setFavorites(favorites);
    }, []);

    useEffect(() => {
        localStorage.setItem("favorites", JSON.stringify(favorites));
	}, [favorites]);

		
	const [favoritesCount, setFavoritesCount] = useState();

	useEffect(() => {
        const favorites = JSON.parse(localStorage.getItem("favorites")|| []);
        setFavoritesCount(favorites.length);
	}, [favorites]);
	
	

	return (

		<PageContainer>
			<Header
				cartsCount={cartsCount}
				favoritesCount={favoritesCount}
			/>

			<ProductList openFirstModal= {openFirstModal}
			      addToFavorites={addToFavorites}    
			 />
			
			<Modal
                header="Додати цей товар у кошик?"
                isOpen={firstModalOpen}
                isClose={closeFirstModal}
                text={selectedProduct.title}
                actions={[
                    {
                        label: 'Ok',
						onClick: () => {
							addToCart()
							closeFirstModal()
						},						
						backgroundColor: 'deepskyblue'
                    },
                    {
                        label: 'Cancel',
						onClick: () => closeFirstModal(),
						backgroundColor: 'gray'
                    }
                ]}
            />
        </PageContainer>
	)
}

export default App;
