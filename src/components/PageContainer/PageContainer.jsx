function PageContainer({children}) {
    return(
        <div className="main">
            {children}
        </div>
    )
}

export default PageContainer;