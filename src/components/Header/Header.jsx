// import { useState, useEffect } from 'react'
import  './Header.scss'
import {ReactComponent as Logo} from "./icons/logo.svg";
import {ReactComponent as Basket} from "./icons/basket-shopping-solid.svg";
import {ReactComponent as Star} from "./icons/star-solid.svg";

function Header({cartsCount, favoritesCount}) {

    
    return(
        <header className="header">
            <div className="container">
                <a href="#"><Logo/></a>
                <div className="icon-container">
                    <a className="icon-header"><Basket/></a>
                    <span>{cartsCount}</span>    
                    <a className="icon-header"><Star/></a>
                    <span>{favoritesCount}</span>
                </div>
            </div>
            
        </header>
    )
}

export default Header