import { useState, useEffect } from 'react';
import '../Product.scss'
import PropTypes from 'prop-types'


function Product(props) {
    
    const {title, price, number, id, image, openModal, addToFavorites} = props;
    
    const [isFavorite, setIsFavorite] = useState(false);

    const toggleFavorite = () => {
        const newIsFavorite = !isFavorite;
        setIsFavorite(newIsFavorite);
        
        addToFavorites(newIsFavorite); 
    };

    useEffect(() => {
        const storedIsFavorite = JSON.parse(localStorage.getItem(`favorite_${id}`)) || false;
        setIsFavorite(storedIsFavorite);
    }, [id]);

    useEffect(() => {
        localStorage.setItem(`favorite_${id}`, JSON.stringify(isFavorite));
    }, [id, isFavorite]);

      
    
    return(
        <li className="item">
            <img className="image" src={image} alt="img" />
            <h2 >{title}</h2>
            <p >Вартість: {price} грн.</p>
            <p >Артикул: {number}</p>
            <div className="buttonWrapper-product">
                <button className="button" onClick={openModal}>Замовити</button>
                <button className="buttonStar"
                    onClick={toggleFavorite}
                >
                    <svg className={isFavorite ? "favorite" : "icon-product"} width="32" height="32" viewBox="0 0 32 32">
                        <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                        
                    </svg>
                </button>
            </div>
            
        </li>
    )
}

Product.propTypes = {
    title: PropTypes.string.isRequired,
    price: PropTypes.number,
    number: PropTypes.string,
    image: PropTypes.string.isRequired,
}

Product.defaultProps = {
    price: 0,
    number: '000',
}

export default Product