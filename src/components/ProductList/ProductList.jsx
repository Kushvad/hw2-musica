import { useState, useEffect } from 'react';
import Product from './Product/Product';
import './Product.scss'

function ProductList(props){
    const [products, setProducts] = useState([])

    useEffect(() => {

		const url = "./products.json"
        fetch(url).then(resp => resp.json()).then(data => setProducts(data.products))

	}, [])



    return(
        <ul className="list">
        {products.map(product =>
            <Product
                title={product.title}
                price={product.price}
                number={product.number}
                id={product.id}
                key={product.id}
                image={product.image}
                openModal={() => props.openFirstModal(product)}
                addToFavorites={() => props.addToFavorites(product)}
            />
        )}
    </ul>
    )
}

export default ProductList;